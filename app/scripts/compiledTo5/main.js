'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var _get = function get(_x4, _x5, _x6) { var _again = true; _function: while (_again) { var object = _x4, property = _x5, receiver = _x6; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x4 = parent; _x5 = property; _x6 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

console.log('\'ממתק \'אמא!');
/*
  NOTE: Babel is enabled, which translates much of the below to ES5 compatible code.
  https://github.com/google/traceur-compiler

  An alternative is 6to5: https://6to5.org/

  Helpful docs for getting up to speed on ES6: https://6to5.org/docs/learn-es6/
*/

// Convenience function for logging. Look funny? It won't by the end of this.
var _ = function _(msg) {
  return console.log(msg);
};

/*
 * let is the new var
 */
_('1. LET IS THE NEW VAR');
var someVariable = 'some value';
_(someVariable); // some value

// Value is mutable
_('VALUE IS MUTABLE');
someVariable = 'another value';
_(someVariable); // another value

// This would cause an error, cannot redeclare in the same block/scope.
// let someVariable = 'yet another value';
_('ERROR: let someVariable = \'yet another value\'');

// This is OK, as it's a different block/scope.
function something() {
  var someVariable = 'yet another value';
  // do stuff
}

/*
 * Constants
 * Cannot be changed once they are set.
 */
_('2. CONST: const a = \'Some string\'');
var a = 'Some string';
// The following line would generate an error. 'a' is read only.
// a = 'Another string';

/*
 * Template strings
 */
_('3. TEMPLATE STRINGS');
var firstName = 'Brad';
var lastName = 'Daily';

_('My name is ' + firstName + ' ' + lastName + '.'); // My name is Brad Daily.

// Can be multiline too
var multilineString = 'My name is ' + firstName + ' ' + lastName + '.\nReally nice to meet ya.';

_(multilineString); // My name is Brad Daily.\nReally nice to meet ya.

/*
 * 'Spread' operator
 * Creates an array from all passed arguments
 */
_('4. SPREAD');
var spread = function spread() {
  for (var _len = arguments.length, x = Array(_len), _key = 0; _key < _len; _key++) {
    x[_key] = arguments[_key];
  }

  return x;
};

_(spread(1, 2, 3)); // [1, 2, 3]

/*
 * Fat arrow function syntax
 */
_('5. FAT ARROW FUNCTION');
/* Old way

  var helloWorld = function() {
    return 'Hello world';
  }
*/

var helloWorld = function helloWorld() {
  return 'Hello world.';
};

_(helloWorld()); // Hello world.

// Here's that spread function from earlier
var newSpread = function newSpread() {
  for (var _len2 = arguments.length, x = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    x[_key2] = arguments[_key2];
  }

  return x;
};

_(newSpread(3, 4, 5)); // [3, 4, 5]

// Example using spread operator plus arrow functions.
// add()  will sum any numbers passed to it as arguments
// The reduce() method applies a function against an accumulator and
// each value of the array (from left-to-right) to reduce it to a single value..
var add = function add() {
  for (var _len3 = arguments.length, x = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    x[_key3] = arguments[_key3];
  }

  return x.reduce(function (previous, current) {
    return previous + current;
  });
};
_(add(1, 5, 10, 20)); // 36

// Array sort
_([1, 5, 8, 2, 3, 4].sort(function (a, b) {
  return a < b ? -1 : 1;
})); // [1, 2, 3, 4, 5, 8]

// the nice thing: Lexical this: Arrow functions inherit the scope they are defined in.
// No more var self = this;
// See: this.owner in the sayFruits function
var pantry = {
  owner: 'Jim',
  fruits: ['apple', 'orange', 'pear'],
  sayFruits: function sayFruits() {
    var _this = this;

    this.fruits.forEach(function (fruit) {
      _(_this.owner + '\'s pantry has ' + fruit + 's. see ES5 source!');
    });
  }
};

pantry.sayFruits();
// Jim's pantry has apples
// Jim's pantry has oranges
// Jim's pantry has pears

/*
 * Object literals
 */
var x = 50;
var y = 100;
_('6. OBJECT LITERALS');
/* Old way

  var coordinates = {
    x: x,
    y: y
  }

*/

var coordinates = {
  x: x, y: y
};

_(coordinates); // Object { x: 50, y: 100}

/*
 * Destructuring
 */
_('7. DESTRUCTURING');
var http = function http() {
  return [404, 'Not found'];
};

var _http = http();

var _http2 = _slicedToArray(_http, 2);

var status = _http2[0];
var textResponse = _http2[1];

_(status); // 404
_(textResponse); // Not found

// list matching
var _ref = [1, 2, 3];
var a1 = _ref[0];
var b1 = _ref[2];

_([a1, b1]); // 1,3
/*
 * Classes
 */
_('8. CLASSSSSSSSSSES');

var Person = (function () {
  function Person(firstName, lastName, age) {
    _classCallCheck(this, Person);

    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  // Getters allow for dynamic properties
  // In this example, you can use .fullName instead of .fullName()
  // See the greeting() function below

  _createClass(Person, [{
    key: 'greeting',
    value: function greeting() {
      return 'Hi, my name is ' + this.fullName + ' and I am ' + this.age + ' years old.';
    }
  }, {
    key: 'isAdult',
    value: function isAdult() {
      return this.age >= 18;
    }
  }, {
    key: 'fullName',
    get: function get() {
      return this.firstName + ' ' + this.lastName;
    }
  }]);

  return Person;
})();

var me = new Person('Brad', 'Daily', 35);
_(me.isAdult()); // true
_(me.greeting()); // Hi, my name is Brad Daily and I am 35 years old.
me.age = 36;
_(me.greeting()); // Hi, my name is Brad Daily and I am 36 years old.

// Class extension

var Pirate = (function (_Person) {
  _inherits(Pirate, _Person);

  function Pirate() {
    _classCallCheck(this, Pirate);

    _get(Object.getPrototypeOf(Pirate.prototype), 'constructor', this).apply(this, arguments);
  }

  _createClass(Pirate, [{
    key: 'greeting',
    value: function greeting() {
      return 'Arggh, me name is ' + this.fullName + ' and I\'ve been sailing these here seas for ' + this.age + ' years.';
    }
  }]);

  return Pirate;
})(Person);

var pirate = new Pirate('Brad', 'Daily', 35);
_(pirate.greeting()); // Arggh, me name is Brad Daily and I've been sailing these here seas for 35 years.

// class super

var Singer = (function (_Person2) {
  _inherits(Singer, _Person2);

  function Singer() {
    _classCallCheck(this, Singer);

    _get(Object.getPrototypeOf(Singer.prototype), 'constructor', this).call(this, 'Yehoram', 'Gaon', 'many, many');
  }

  _createClass(Singer, [{
    key: 'greeting',
    value: function greeting() {
      return 'Arggh, me name is ' + this.fullName + ' and I\'ve been here ' + this.age + ' years.';
    }
  }]);

  return Singer;
})(Person);

var singer = new Singer();
_(singer.greeting());

/*
 * Default parameters values
 */
_('9. DEFAULT PARAMETERS VALUES');
function repeat() {
  var toldYa = arguments.length <= 0 || arguments[0] === undefined ? 'Nothin' : arguments[0];

  return 'You told me ' + toldYa + '.';
}

_(repeat()); // You told me Nothin'.
_(repeat("Somethin'")); // You told me Somethin'.

// Arrow version
var repeat2 = function repeat2() {
  var toldYa = arguments.length <= 0 || arguments[0] === undefined ? "Nothin'" : arguments[0];
  return 'You told me ' + toldYa + '.';
};

_(repeat2()); // You told me Nothin'.
_(repeat2("Somethin'")); // You told me Somethin'.

/*
 * Sets
 * A Set is a collection of unique elements
 */
_('10. SETS');
var letters = new Set();
letters.add('A');
_(letters.size); // 1

// Ignores duplicate values
letters.add('A');
_(letters.size); // 1

_(letters.has('A')); // true
_(letters.has('B')); // false

letters.add('B');

var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = letters[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var letter = _step.value;
    _(letter);
  } // Outputs A, then B
} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator['return']) {
      _iterator['return']();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}

_(letters['delete']('Z')); // returns false since Z is was not in letters before delete()
_(letters['delete']('A')); // returns true since A is was in letters before delete()

letters.clear();
_(letters.size); // 0

/*
 * Maps
 * The keys of a Map can be arbitrary values
 */
_('11. MAPS');
var map = new Map();
map.set('string-key', 'I belong to the string-key');

// Keys can be anything, like a DOM element
map.set(document.getElementsByTagName('h1')[0], 'I belong to the first <h1> in the DOM.');

_(map.size); // 2

_(map.get(document.getElementsByTagName('h1')[0])); // I belong to the first <h1> in the DOM.

var _iteratorNormalCompletion2 = true;
var _didIteratorError2 = false;
var _iteratorError2 = undefined;

try {
  for (var _iterator2 = map.entries()[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
    var _step2$value = _slicedToArray(_step2.value, 2);

    var _key4 = _step2$value[0];
    var val = _step2$value[1];

    _(_key4); // Outputs 'string-key', then the <h1>
    _(val); // Outputs 'I belong to the string-key', then 'I belong to the first <h1> in the DOM.'
  }
  /*
  * WeakMaps
  * WeakMap is a Map that doesn’t prevent its keys from being garbage-collected.
  * That means that you can associate private data with objects without having to worry about memory leaks:
  */
} catch (err) {
  _didIteratorError2 = true;
  _iteratorError2 = err;
} finally {
  try {
    if (!_iteratorNormalCompletion2 && _iterator2['return']) {
      _iterator2['return']();
    }
  } finally {
    if (_didIteratorError2) {
      throw _iteratorError2;
    }
  }
}

_('12. WEAK MAP/SET');
var _counter = new WeakMap();
var _action = new WeakMap();

var Countdown = (function () {
  function Countdown(counter, action) {
    _classCallCheck(this, Countdown);

    _counter.set(this, counter);
    _action.set(this, action);
    this.x = 12;
  }

  _createClass(Countdown, [{
    key: 'dec',
    value: function dec() {
      // the counter will receive a number even if this was garbage-collected
      var counter = _counter.get(this);
      console.log('counter1', _counter, counter);
      if (counter < 1) return;
      counter--;
      _counter.set(this, counter);
      // when reach 0, fire the action
      if (counter === 0) {
        _action.get(this)();
      }
    }
  }]);

  return Countdown;
})();

_('12a. COMPUTED METHOD NAMES');
/*
* define the name of a method via an expression, if you put it in square brackets.
*/
var mamashStam = 'myMethod called';

var Stam = (function () {
  function Stam() {
    _classCallCheck(this, Stam);
  }

  _createClass(Stam, [{
    key: 'myMethod',
    value: function myMethod() {
      _(mamashStam);
    }
  }]);

  return Stam;
})();

var m = 'myMethod';

var Stamy = (function () {
  function Stamy() {
    _classCallCheck(this, Stamy);
  }

  _createClass(Stamy, [{
    key: m,
    value: function value() {
      _(mamashStam);
    }
  }]);

  return Stamy;
})();

var stam = new Stam();
var stamy = new Stamy();
_(stam.myMethod() === stamy.myMethod());
_(stamy.myMethod());

_('14. ITERATORS + FOR..OF');
// if an object has Symbol.iterator named function, it is iteratable by for..of
var fibonacci = _defineProperty({}, Symbol.iterator, function () {
  var pre = 0,
      cur = 1;
  return {
    next: function next() {
      var _ref2 = [cur, pre + cur];
      pre = _ref2[0];
      cur = _ref2[1];

      return { done: false, value: cur };
    }
  };
});

var _iteratorNormalCompletion3 = true;
var _didIteratorError3 = false;
var _iteratorError3 = undefined;

try {
  for (var _iterator3 = fibonacci[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
    var n = _step3.value;

    // truncate the sequence at 1000
    if (n > 10) break;
    _(n);
  }
} catch (err) {
  _didIteratorError3 = true;
  _iteratorError3 = err;
} finally {
  try {
    if (!_iteratorNormalCompletion3 && _iterator3['return']) {
      _iterator3['return']();
    }
  } finally {
    if (_didIteratorError3) {
      throw _iteratorError3;
    }
  }
}

_('15. GENERATORS');
// Generators simplify iterator-authoring using function* and yield.
// They include the next() within them
// var fibonacci_star = {
//   [Symbol.iterator]: function*() {
//     var pre = 0, cur = 1;
//     for (;;) {
//       var temp = pre;
//       pre = cur;
//       cur += temp;
//       yield cur;
//     }
//   }
// }
//
// for (var n of fibonacci_star) {
//   // truncate the sequence at 1000
//   if (n > 10)
//     break;
//   _(n);
// }

_('16. UNICODE');
// More Unicode support
// like es5
var shin = "ש";
_('ש');
// new
var shinbet = 'ש';
_(shinbet);
_(shin.length == shinbet.length);

_('17. PROXIES');
//Meta programming
// intercepting calls on object
// unsupported by Babel
var target = {};
var handler = {
  /** Intercepts: getting properties */
  get: function get(target, propKey, receiver) {
    _('GET ' + propKey);
    return 123;
  },

  /** Intercepts: checking whether properties exist */
  has: function has(target, propKey) {
    _('HAS ' + propKey);
    return true;
  }
};
//let proxy = new Proxy(target, handler);
//proxy.stam(); // GET stam
// (function() {
//   'shalom' in proxy; // HAS shalom
// }());

_('18. SYMBOLS');
// a key does not have to be a string
// global scoped symbol
var key = Symbol("mykey");

function MyClass(privateData) {
  this[key] = privateData;
}

MyClass.prototype = {
  doStuff: function doStuff() {
    _(this[key]);
  }
};

_(typeof key === "symbol");
// Limited support from Babel, full support requires native implementation.
var c = new MyClass("hello");
c.doStuff();
_(c["mykey"] === undefined);
_(c[key] === "hello");

_('19. Math + Number + String + Object APIs');
// Many new library additions, including core Math libraries,
// Array conversion helpers, and Object.assign for copying.

/*unsupported by Babel _(Number.EPSILON)
_(Number.isInteger(Math.Infinity) // false
_(Number.isNaN("NaN")) // false
*/
_(Math.acosh(3)); // 1.762747174039086
_(Math.hypot(3, 4)); // 5
_(Math.imul(Math.pow(2, 32) - 1, Math.pow(2, 32) - 2)); // 2

_("abcde".includes("cd")); // true
_("abc".repeat(3)); // "abcabcabc"

_(Array.from(document.querySelectorAll("*"))); // Returns a real Array
_(Array.of(1, 2, 3)); // Similar to new Array(...), but without special one-arg behavior
_([0, 0, 0].fill(7, 1)); // [0,7,7]
_([1, 2, 3].findIndex(function (x) {
  return x == 2;
})); // 1
_(["a", "b", "c"].entries()); // iterator [0, "a"], [1,"b"], [2,"c"]
_(["a", "b", "c"].keys()); // iterator 0, 1, 2
///_(["a", "b", "c"].values()) // iterator "a", "b", "c"  unsupported by Babel

var Point = function Point(x, y) {
  _classCallCheck(this, Point);

  var _ref3 = [x, y];
  this.x = _ref3[0];
  this.y = _ref3[1];
};

var point = new Point(9, 8);
Object.assign(point, { origin: new Point(0, 0) });
_(point); //Point {x: 9, y: 8, origin: 0,0}

_('20. PROMISES');
function timeout() {
  var duration = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];

  return new Promise(function (resolve, reject) {
    // call resolve after duractiom
    setTimeout(resolve, duration);
  });
}

var p = timeout(1000).then(function () {
  _('resolved called');
  return timeout(2000); // create another promise
}, function () {
  _('reject called');
  throw new Error("hmm");
}).then(function () {
  _('second then  resolved');return 12;
}).then(function () {
  _('third chained');
});